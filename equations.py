# -*- coding: UTF-8 -*-

import math
import cmath


class Equation:
    def __init__(self):
        self._a = 0
        self._b = 0
        self._c = 0
        self._r = 0

    @property
    def a(self):
        return self._a

    @a.setter
    def a(self, value):
        self._a = float(value)

    @property
    def b(self):
        return self._b

    @b.setter
    def b(self, value):
        self._b = float(value)

    @property
    def c(self):
        return self._c

    @c.setter
    def c(self, value):
        self._c = float(value)

    @property
    def r(self):
        return self._r

    @r.setter
    def r(self, value):
        self._r = float(value)

    def __str__(self):
        return "%.2fx2 + (%.2fx) + (%.2f) = %.2f" % (self.a, self.b, self.c, self.r)

    @property
    def discriminant(self):
        return self.b ** 2 - 4 * self.a * self.c

    def normalize(self):
        if self.a == 0:
            raise WrongEquationError

        if self.r != 0:
            self.c -= self.r
            self.r = 0

    def solve(self):
        raise NotImplementedError


class QuadraticEquation(Equation):
    def solve(self):
        self.normalize()

        if self.b != 0 and self.c != 0:
            return self._solve_full_equation()
        elif self.b == 0 and self.c != 0:
            return self._b_is_zero()
        elif self.b != 0 and self.c == 0:
            return self._c_is_zero()
        else:
            return 0    # b and c are zero

    def _solve_full_equation(self):
        if self.discriminant > 0:
            x1 = (-self.b + math.sqrt(self.discriminant)) / (2 * self.a)
            x2 = (-self.b - math.sqrt(self.discriminant)) / (2 * self.a)
            return x1, x2
        elif self.discriminant == 0:
            return -self.b / (2 * self.a)
        else:
            x1 = complex(-self.b, cmath.sqrt(abs(self.discriminant))) / (2 * self.a)
            x2 = complex(-self.b, -cmath.sqrt(abs(self.discriminant))) / (2 * self.a)
            return x1, x2

    def _b_is_zero(self):
        c_by_a = -self.c / self.a

        if c_by_a > 0:
            x1 = math.sqrt(c_by_a)
            x2 = -x1
            return x1, x2
        elif c_by_a == 0:
            return 0
        else:
            x1 = complex(0, cmath.sqrt(abs(c_by_a)))
            x2 = -x1
            return x1, x2

    def _c_is_zero(self):
        x1 = 0
        x2 = -self.b / self.a
        return x1, x2


class ExceptionalEquation(Equation):
    def solve(self):
        self.normalize()

        if self.b == 0 and self.c == 0:
            raise SolvingError('"x" может быть любым числом.')
        elif self.b == 0 and self.c != 0:
            raise SolvingError("Нет корней.")
        elif self.b != 0 and self.c == 0:
            return 0
        elif self.b != 0 and self.c != 0:
            return -self.c / self.b

    def normalize(self):
        if self.r != 0:
            self.c -= self.r
            self.r = 0


class EquationParser:
    def __init__(self):
        raise AbstractClassError

    @staticmethod
    def parse(str_equation):
        import re

        spaceless_equation = re.sub("\s", "", str_equation)

        equality_sign_pos = spaceless_equation.find("=")
        if equality_sign_pos == -1:
            r = 0.0
        else:
            r = float(spaceless_equation[(equality_sign_pos + 1):])
            spaceless_equation = spaceless_equation[:equality_sign_pos]

        if spaceless_equation[0] not in ("+", "-"):
            spaceless_equation = "+" + spaceless_equation
        pairs = re.split("([+\-])", spaceless_equation)
        del pairs[0]

        it = iter(pairs)
        operands = [sign + number for sign, number in zip(it, it)]

        a, b, c, r = [], [], [-r], 0

        try:
            for operand in operands:
                if operand[-2:] == "x2":
                    coeff = 1 if operand == "+x2" else -1 if operand == "-x2" else operand[:-2]
                    a.append(float(coeff))
                elif operand[-3:] == "x^2":
                    coeff = 1 if operand == "+x^2" else -1 if operand == "-x^2" else operand[:-3]
                    a.append(float(coeff))
                elif operand[-1] == "x":
                    coeff = 1 if operand == "+x" else -1 if operand == "-x" else operand[:-1]
                    b.append(float(coeff))
                else:
                    c.append(float(operand))
        except ValueError:
            raise WrongEquationError

        a, b, c = sum(a), sum(b), sum(c)

        if a != 0:
            equation = QuadraticEquation()
        else:
            equation = ExceptionalEquation()

        equation.a = a
        equation.b = b
        equation.c = c
        equation.r = r

        return equation


class WrongEquationError(AttributeError):
    pass


class SolvingError(RuntimeError):
    def __init__(self, message):
        self.message = message


class AbstractClassError(BaseException):
    pass
