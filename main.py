#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import re
from equations import *


# Hack needed for compatibility with Python 2
try:
    input = raw_input
except NameError:
    pass


print('Добро пожаловать в dev-версию программы по решению квадратных уравнений от Леонида "Kozalo" Козарина!')
print('Пожалуйста, следуйте указаниям программы или введите "Ой, всё!" для выхода (без кавычек).')

while True:
    user_equation = input("\nВведите уравнение: ")
    if re.match("Ой,? всё!?", user_equation, re.IGNORECASE):
        break

    print('')
    try:
        equation = EquationParser.parse(user_equation)
    except WrongEquationError:
        print("Введённое Вами уравнение не является квадратным!\n")
        continue

    print("Уравнение: %s." % equation)
    print("Дискриминант: %s." % equation.discriminant)

    print('')
    try:
        print("Корень(-ни): %s." % str(equation.solve()))
    except SolvingError as err:
        print(err.message)
    print('')
