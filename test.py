#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from equations import *
from datetime import datetime


TIME_FORMAT = '{:%H:%M:%S:%f}'

test_dict = {
    '000': '0=0',
    '001': '1=0',
    '010': 'x=0',
    '011': 'x+1=0',
    '100': 'x2=0',
    '101': 'x2+1=0',
    '110': 'x2+x=0',
    '111': 'x2+x+1=0',
    'ttt': 'tx2+tx+t=0'
}

consumed_time = []
counter = 1
for case, eq in sorted(test_dict.items()):
    print("\nУравнение №%i: %s (%s)" % (counter, eq, case))

    start_time = datetime.now()

    try:
        equation = EquationParser.parse(eq)
    except WrongEquationError:
        print("Введённое Вами уравнение не является квадратным!\n")

        consumed_time.append({
            'label': case,
            'start': start_time,
            'finish': datetime.now()
        })
        counter += 1
        continue

    print("Уравнение: %s." % equation)
    print("Дискриминант: %s." % equation.discriminant)

    print('')
    try:
        print("Корень(-ни): %s." % str(equation.solve()))
    except SolvingError as err:
        print(err.message)
    print('')

    consumed_time.append({
        'label': case,
        'start': start_time,
        'finish': datetime.now()
    })
    counter += 1

first_test_start_time = consumed_time[0]['start']
last_test_finish_time = consumed_time[len(consumed_time)-1]['finish']
print(("Время начала теста: %s" % TIME_FORMAT).format(first_test_start_time))
print(("Время окончания теста: %s" % TIME_FORMAT).format(last_test_finish_time))
total_consumed_time = last_test_finish_time - first_test_start_time
print("Затраченное время: %s" % total_consumed_time)

print('')
table_heading = "| Случай | Время начала    | Время окончания | Затраченное время |"
print('-' * len(table_heading))
print(table_heading)
print('-' * len(table_heading))
for case in consumed_time:
    consumed_time = case['finish'] - case['start']
    timeless_string = "| %-6s | %s | %s | %-17s |" % (case['label'], TIME_FORMAT, TIME_FORMAT, consumed_time)
    print(timeless_string.format(case['start'], case['finish']))
print('-' * len(table_heading))
